# adeci_exam

Please see below instructions for your exam

1. Check photoshop for the mockup design.

2. Convert template into working html
	- Use tools like bootstrap, jquery, html5, etc.
	- Make it responsive and use standard semantic codes.

3. The time you receive this email, your timeline will start and you should submit this exam 2 days from now. Send us your git repository link of your finish exam.

4. After you send your repository link with your working html. Give us 2 days to review your exam and after that we will schedule you for another final interview.

*Note: You are free to do research and development for your exam.

Link to psd file: https://drive.google.com/file/d/1cCdp5obOsIZvkwLidJ1X_ilAl2oHbFkm/view?usp=sharing

Please send your exam to the ff emails:

mark.pacheco@adec-group.com
christopher.din@adec-group.com
